// import { StatusBar } from 'expo-status-bar';
// import React,{ useEffect, useRef, useState} from 'react';
// import { StyleSheet, Text, View, Button, SafeAreaView, ScrollView, Image } from 'react-native';
// // import {
// //   useAnimatedStyle,
// //   useSharedValue,
// //   withTiming,
// // } from 'react-native-reanimated';
// // import { Animated } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native';
// // import * as React from 'react';
// import { createDrawerNavigator } from '@react-navigation/drawer';
// import SavedScreen from './Saved';
// import ReferScreen from './ReferScreen';
// import DrawerItems from './DrawerItems';
// import ProfileScreen from './ProfileScreen';
// import SettingsScreen from './Settings';

// import { CopilotProvider } from 'react-native-copilot';
// import {  walkthroughable, CopilotStep } from 'react-native-copilot';
// const WalkthroughableText = walkthroughable(Text);
// const WalkthroughableImage = walkthroughable(Image);

// const Drawer = createDrawerNavigator();
// import DATA from './data';
// // const HEADER_HEIGHT = 200;
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   parent: {
//     flex: 1,
//     paddingTop: 40,
//     paddingHorizontal: 20
//   },
//   header:{
//    fontSize: 24,
//    marginBottom: 20,
//    textAlign: 'center',
//   },
//   box:{
//    backgroundColor: '#000',
//    borderRadius: 15,
//    padding: 20 
//   },
//   textBody:{
//     fontSize: 20,
//     marginBottom:20,
//     color: '#fff',
//   },
//   btnViewMore:{
//     backgroundColor: '#fff',
//     width: 100,
//     color: '#fff',
//   },
//   fadeinContainer:{
//     padding: 20,
//     backgroundColor: 'powderblue'
//   },
//   title: {
//     fontSize: 24,
//     textAlign: 'center',
//   },
// });
// const App = () => {


//   // useEffect(()=>{
//   //   props.start();
//   // },[])

//   // const boxHeight = useSharedValue(60);
//   // const [maxLines, setMaxLines] = useState(2);
//   // const fadeinAnim = useRef(new Animated.Value(0)).current;
//   // const offset = useRef(new Animated.Value(0)).current;

//   // const truncatedAnimation = useAnimatedStyle(() =>{
//   //   return{
//   //     height: withTiming(boxHeight.value,{ duration: 1000})
//   //   }
//   // },[])

//   // function showText() {
//   //   setTimeout(() => {
//   //     maxLines === 2 ? setMaxLines(0) : setMaxLines(2);
//   //   }, 400);
//   //   boxHeight.value === 60 ? (boxHeight.value = 500) : (boxHeight.value = 60);
//   // }

//   // const fadeInAnim = () => {
//   //   Animated.timing(fadeinAnim,{
//   //     toValue: 1,
//   //     duration: 1000,
//   //     useNativeDriver: true,
//   //   }).start();
//   // }

//   // const insets = useSafeAreaInsets();

//   // const headerHeight = offset.interpolate({
//   //   inputRange: [0, HEADER_HEIGHT + insets.top],
//   //   outputRange: [HEADER_HEIGHT + insets.top, insets.top + 44],
//   //   extrapolate: 'clamp'
//   // });

//   // const getHeader = ()=> {
//   //   return(<Animated.View style={{position:'absolute',top:0,left:0,right:0,zIndex:10,height:headerHeight,backgroundColor:'lightblue'}}>
       
//   //   </Animated.View>)
//   // } 
  
//   return(<>
//   <CopilotProvider overlay="svg" backdropColor="rgba(50, 50, 100, 0.9)" > 

//     {/* <View style={{  backgroundColor: 'white' }}></View> */}
//      <NavigationContainer style={{}}>
//     <Drawer.Navigator
//        drawerType="front"
//        initialRouteName="Profile"
//        screenOptions={{drawerPosition:"right"}}
//       //  drawerContentOptions={{
//       //    activeTintColor: '#e91e63',
//       //    itemStyle: { marginVertical: 10 },
//       //  }}

// >
//        {
//          DrawerItems.map(drawer=><Drawer.Screen
//            key={drawer.name}
//            name={drawer.name}
//            options={{
           
//                headerShown:true,
//                }

//            }
//            component={
//              drawer.name==='Profile' ? ProfileScreen
//                : drawer.name==='Settings' ? SettingsScreen
//                  : drawer.name==='Saved Items' ? SavedScreen
//                    : ReferScreen
//            }
//          />)
//        }
// </Drawer.Navigator>
// </NavigationContainer>
//     {/* <View style={{ height: '100%',width: '100%', position: 'absolute',
//     zIndex: 1,}}>
//    <CopilotStep text="Hey! This is the first step of the tour!" order={1} name="openApp">
//       <WalkthroughableText style={styles.title}>
//         {'Welcome to the demo of\n"React Native Joyride"'}
//       </WalkthroughableText>
//     </CopilotStep>
//     </View>   */}


// </CopilotProvider>
//   </>)

//   // return (
//     // <SafeAreaView style={styles.parent}>
//       {/* {getHeader()}
//       <ScrollView style={{ flex:1, backgroundColor: 'white'}} contentContainerStyle={{alignItems:'center',paddingTop:220,paddingHorizontal:20}}
//       showsVerticalScrollIndicator={false} scrollEventThrottle={16} onScroll={Animated.event([
//         {nativeEvent: {contentOffset:{y:offset}}}
//       ],{useNativeDriver:false})}>
//         {DATA.map(item=>(
//           <View key={item.id} style={{ marginBottom:20 }}>
//             <Text style={{ color: '#101010', fontSize: 12 }}>{item.title}</Text>
//           </View>
//         ))}
//       </ScrollView> */}
//       {/* <Text>Open up App.js to start working on your app!</Text> */}
//       {/* <StatusBar style="auto" /> */}
//       {/* <Text style={styles.header}>React Native Reanimated Tutorial</Text> */}
//       {/* <Animated.View style={[ styles.fadeinContainer,{ opacity: fadeinAnim}]}></Animated.View> */}
//       {/* <View style={styles.box}> */}
//         {/* <Animated.View style={[{ marginTop:20}, truncatedAnimation]} >
//         <Text style={styles.textBody} numberOfLines={maxLines}>
//         Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur
//             magnam necessitatibus dolores qui sunt? Mollitia nostrum placeat
//             esse commodi modi quaerat, et alias minima, eligendi ipsa
//             perspiciatis, totam quod dolorum.
//             {'\n'}
//             {'\n'}
//             Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur
//             magnam necessitatibus dolores qui sunt? Mollitia nostrum placeat
//             esse commodi modi quaerat, et alias minima, eligendi ipsa
//             perspiciatis, totam quod dolorum.
//         </Text>
//         </Animated.View>
//         <Button style={styles.btnViewMore}  title={maxLines === 2 ? 'View more' : 'Close Dropdown'}
//           onPress={showText}></Button> */}
        
//       {/* </View> */}
//     // </SafeAreaView>
//   // );
// };

// export default App;


// Example to Create Step by Step Walkthrough in any React Native App
// https://aboutreact.com/step-by-step-app-introduction-in-react-native/

// import React in our code
import React, {useState, useEffect} from 'react';

// import all the components we are going to use
import {
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  Switch,
} from 'react-native';

// For Step by Step Walkthrough
import {
  CopilotProvider,
  walkthroughable,
  CopilotStep
} from 'react-native-copilot';
import { useCopilot } from "react-native-copilot";

const App = () => {
  const [secondStepActive, setSecondStepActive] = useState(true);
  const { copilotEvents } = useCopilot();

  useEffect(() => {
    //setting a function to handle the step change event
    // props.copilotEvents.on('stepChange', handleStepChange);
    //To start the step by step Walk through
    // props.start();
    const listener = () => {
      // Copilot tutorial finished!
    };

    // copilotEvents.on("start", listener);

    return () => {
      // copilotEvents.off("start", listener)
    };
  }, []);

  const handleStepChange = (step) => {
    //Handler, in case we want to handle the step change
    console.log(`Current step is: ${step.name}`);
  };

  //Making a WalkthroughableText
  const WalkthroughableText = walkthroughable(Text);
  //Making a WalkthroughableImage
  const WalkthroughableImage = walkthroughable(Image);

  return (
   
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        {/*Step 1 to show the heading*/}
        <CopilotStep
          text="This is the heading with some style"
          order={1}
          name="firstUniqueKey">
          <WalkthroughableText style={styles.title}>
            Example of App Introduction Tour in React Native
          </WalkthroughableText>
        </CopilotStep>
        {/*Step 3 to show the Image*/}
        <CopilotStep 
          text="This is a image"
          order={3}
          name="thirdUniqueKey">
          <WalkthroughableImage
            source={{
              uri:
                'https://raw.githubusercontent.com/AboutReact/sampleresource/master/react_logo.png',
            }}
            style={styles.profilePhoto}
          />
        </CopilotStep>
        {/*Step 2 to show the normal text*/}
        <View style={styles.activeSwitchContainer}>
          <CopilotStep
            active={secondStepActive}
            text="This is simple text without style"
            order={2}
            name="SecondUniqueKey">
            <WalkthroughableText>
              Default text without style which can be skiped
              after disabling the switch
            </WalkthroughableText>
          </CopilotStep>
          <View style={{flexGrow: 1}} />
          <Switch
            onValueChange={(secondStepActive) =>
              setSecondStepActive(secondStepActive)
            }
            value={secondStepActive}
          />
        </View>
        {/*Button to start the walkthrough*/}
        <View style={styles.middleView}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => props.start()}>
            <Text style={styles.buttonText}>
              START APP INTRODUCTION TOUR
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 40,
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
    margin: 20,
  },
  profilePhoto: {
    width: 140,
    height: 140,
    borderRadius: 70,
    marginVertical: 20,
  },
  middleView: {
    flex: 1,
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#2980b9',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  activeSwitchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
    alignItems: 'center',
    paddingHorizontal: 40,
  },
});


