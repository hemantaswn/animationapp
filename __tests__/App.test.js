import React from 'react';
import {render, screen} from '@testing-library/react-native';
// import Hello from './Hello';
import App from '../App';

describe('Hello', () => {
  it('renders the correct message', () => {
    render(<App />);
    expect(screen.getByText('Hello, world!')).toBeVisible();
  });
});