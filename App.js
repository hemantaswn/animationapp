import { StyleSheet, View, Text } from 'react-native';
// import Mapbox from '@rnmapbox/maps';

// Mapbox.setAccessToken('pk.eyJ1IjoiZGVtbzI1MSIsImEiOiJjbDdkdGs1Z2MwMTcyM3ZwZGN6cHJ1bXZ0In0.507RZa1vt38mN9ZZwPrtOg');

const App = () => {
  return (
    <View style={styles.page}>
      <View style={styles.container}>
      <Text>Hello, world!</Text>
        {/* <Mapbox.MapView style={styles.map} /> */}
      </View>
    </View>
  );
}

export default App;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    height: '100%',
    width: '100%',
  },
  map: {
    flex: 1
  }
});